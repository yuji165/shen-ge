﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeCall : Graphic
{
    private const float 
        MaxRange = 1f,
        MaxAlpha = 255;

    [SerializeField]
    private Texture fadeMaskTexture = null;
    [SerializeField, Range(0, MaxRange)]
    private float cutRange = 0;
    [SerializeField, Range(0, 10)]
    public float FadeTimer = 2;
    //[SerializeField]
    //private Canvas uiCanvas = null;

    private bool 
        isFadeOut = false,
        isFadeIn = false,
        isFadeEnd = false;

    protected override void Start()
    {
        base.Start();
        UpdateMaskTexture(fadeMaskTexture);
    }
#if UNITY_EDITOR
    protected override void OnValidate()
    {
        base.OnValidate();
        UpdateMaskRange(cutRange);
        UpdateMaskTexture(fadeMaskTexture);
    }
#endif
    private void UpdateMaskRange(float range)
    {
        enabled = true;
        material.SetFloat("_Range", MaxRange - range);
        if (range <= 0)
            enabled = false;
    }

    /// <summary>
    /// テクスチャの更新
    /// </summary>
    /// <param name="texture">更新するテクスチャ</param>
    private void UpdateMaskTexture(Texture texture)
    {
        material.SetTexture("_MaskTex", texture);
        material.SetColor("_Color", color);
    }
    private void Update()
    {
        if (isFadeOut)
        {
            //FadeTimerに合わせて徐々にフェードさせていく
            if (cutRange < 1)
            {
                cutRange += Time.deltaTime / FadeTimer;
                color = new Color(color.r,color.g,color.b,cutRange);
            }
            else
            {
                cutRange = Mathf.Floor(1);
                SceneChanger.Faded = true;
                isFadeOut = false;
            }
        }
        if (isFadeIn)
        {
            if (cutRange > 0)
            {
                cutRange -= Time.deltaTime / FadeTimer;
                color = new Color(color.r, color.g, color.b, cutRange);
            }

            //上のifでcutRangeが0以下になった時ifelse処理だとelseにたどり着く前に
            //UpdateMaskRangeでスクリプトが enabled = false になってelseが作動しなくなるのでifを分ける必要がある
            if (cutRange <= 0)
            {
                cutRange = Mathf.Floor(0);
                SceneChanger.Faded = true;
                isFadeIn = false;
                isFadeEnd = true;
            }
        }
        //更新
        UpdateMaskRange(cutRange);
        UpdateMaskTexture(fadeMaskTexture);
        //フェードが終わったら消す
        if (isFadeEnd)
            Destroy(gameObject);
    }

    /// <summary>
    /// フェードの呼び出し
    /// </summary>
    public void FadeOutCall()
    {
        DontDestroyOnLoad(gameObject);
        isFadeOut = true;
        //更新しないとUpdateが呼び出されない
        cutRange += Time.deltaTime / FadeTimer;
        UpdateMaskRange(cutRange);
        UpdateMaskTexture(fadeMaskTexture);
        SceneChanger.Faded = false;
    }
    public void FadeInCall()
    {
        isFadeIn = true;
        SceneChanger.Faded = false;
    }
}