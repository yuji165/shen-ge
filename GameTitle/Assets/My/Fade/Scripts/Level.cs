﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public enum LEVEL
    {
        one,
        two,
        three,
        max
    }
    public static LEVEL level{ get; private set;}
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        level = LEVEL.max;
    }

    public void SetLevel()
    {

    }
}