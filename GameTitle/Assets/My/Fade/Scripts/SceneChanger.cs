﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    [SerializeField]
    private GameObject
        fadeCube = null,
        startItem = null;
    [SerializeField]
    private Vector3
        itemPoint;
    FadeCall fadeCall;
    private AsyncOperation
        async;
    private bool
        gameStartNow = false;
    private GameObject[]
        titleItem;
    public enum SCENE_NAME
    {
        Title,
        MyVR,
        max
    }
    public static bool Faded
    {
        get;
        set;
    }
    public static bool SceneChangeNow
    {
        get;
        set;
    }
    public static string PresentSceneName
    {
        get;
        private set;
    }
    /// <summary>
    /// 次のシーンを非同期で呼び出す
    /// </summary>
    /// <param name="name">シーン移行先のシーン名</param>
    private void SceneChange(string name)
    {
        //フェードアウト開始
        GameObject fadeObject = Instantiate(fadeCube, GameObject.FindGameObjectWithTag("Player").transform.position, Quaternion.identity);
        DontDestroyOnLoad(fadeObject);
        fadeCall = fadeObject.GetComponent<FadeCall>();
        fadeCall.FadeOutCall();

        //StartCoroutine(WaitLoad(fadeCall.FadeTimer, name));

        SceneManager.activeSceneChanged += ActiveSceneChanged;
        async = SceneManager.LoadSceneAsync(name);
        async.allowSceneActivation = false;
        Invoke("WaitLoad", fadeCall.FadeTimer * 2);
    }

    private void ActiveSceneChanged(Scene thisScene, Scene nextScene)
    {
        Debug.Log("シーン移行完了");
        PresentSceneName = nextScene.name;
        fadeCall.FadeInCall();
        if (nextScene.name == SCENE_NAME.Title.ToString())
            GameStartManager();
    }
    /// <summary>
    /// 待機中処理
    /// </summary>
    private void WaitLoad()
    {
        if (async.progress >= 0.9f)
        {
            async.allowSceneActivation = true;
            Debug.Log("ロード終了");
        }
        else
            Invoke("WaitLoad", 1);
    }
    private void GameStartManager()
    {
        if (!gameStartNow)
        {
            Instantiate(startItem, itemPoint, Quaternion.identity);
            TitleItemActive(false);
        }
    }
    public void TitleItemActive(bool active)
    {
        if (!active)
            titleItem = GameObject.FindGameObjectsWithTag("TitleItem");
        for (int loop = 0; loop < titleItem.Length; loop++)
        {
            titleItem[loop].SetActive(active);
        }
    }
    #region こっちだとロードできない...
    private IEnumerator WaitLoad(float waitTime, string name)
    {
        async = SceneManager.LoadSceneAsync(name.ToString());
        async.allowSceneActivation = false;
        //while (async.progress < 0.9)
        //{
        //    //ロード中の処理
        //    yield return 0;
        //}

        //0.9で止まるようになっているので
        if (async.progress >= 0.9f)
        {
            if (Faded)
            {
                async.allowSceneActivation = true;
            }
            else
            {
                yield return new WaitForSeconds(waitTime);
                async.allowSceneActivation = true;
            }
            Debug.Log("ロード終了");
        }
    }
    #endregion
    //唯一性を守るため同じオブジェクトがあれば消える
    private void Awake()
    {
        if (GameObject.Find(gameObject.name) != gameObject)
            Destroy(gameObject);
    }
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        Faded = false;
        SceneChangeNow = false;
        PresentSceneName = SceneManager.GetActiveScene().name;
        GameStartManager();
    }
    private void Update()
    {
        if (SceneChangeNow)
        {
            if (PresentSceneName == SCENE_NAME.Title.ToString())
                SceneChange(SCENE_NAME.MyVR.ToString());
            SceneChangeNow = false;
        }
    }
}
