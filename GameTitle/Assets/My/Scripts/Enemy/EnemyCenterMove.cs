﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCenterMove : MonoBehaviour
{
    public float speed;
    public float defDistance;
    public float rotaDistance;
    public float disDistance;
    private float distance;
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        distance = defDistance;
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(Vector3.Distance(player.transform.position, transform.position)) > 10)
        {
            Vector3 target;
            if (Mathf.Abs(Vector3.Distance(player.transform.position, transform.position)) < rotaDistance)
            {
                target = player.transform.position;
                distance = defDistance;
            }
            else
            {
                float rad = Mathf.Atan2(player.transform.position.z - transform.position.z, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
                Vector2 targetAngle =
                    rad <= 0 && rad > -90 ? Vector2.up :
                    rad <= -90 ? Vector2.right :
                    rad > 0 && rad < 90 ? Vector2.left : Vector2.down;

                target = player.transform.position + new Vector3(targetAngle.x, 0, targetAngle.y) * distance;
                distance -= disDistance;
            }
            Vector3 targetDir = target - transform.position;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, 0.7f * Time.deltaTime, 0f);
            transform.rotation = Quaternion.LookRotation(newDir);
            transform.position += transform.TransformDirection(Vector3.forward) * speed;
        }
    }
}
