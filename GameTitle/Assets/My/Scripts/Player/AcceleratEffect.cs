﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceleratEffect : MonoBehaviour
{
    private GameObject player;
    private ParticleSystem particleSystem;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        particleSystem = GetComponent<ParticleSystem>();
        particleSystem.enableEmission = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.GetComponent<PlayerMove>().Speed > 1.2f)
        {
            transform.position = player.transform.position + player.GetComponent<PlayerMove>().MoveVec * 5;
            transform.LookAt(player.transform.position);
            particleSystem.enableEmission = true;
        }
        else
        {
            particleSystem.enableEmission = false;
        }
    }
}
