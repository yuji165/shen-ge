﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour
{
    [SerializeField]
    private GameObject 
        inObject = null,
        inObjectChild = null;
    [SerializeField]
    private float speed = 1;
    private Vector3 playerMove;
    private Material inObjectMaterial;
    private bool inObjectNow;
    public enum Finish { clear, gameOver, no, sleep }
    public Finish Finishd
    {
        get; set;
    }
    // Start is called before the first frame update
    void Start()
    {
        Finishd = Finish.no;
        inObjectNow = false;
        inObjectMaterial = inObjectChild.GetComponent<Renderer>().material;
        inObjectMaterial.SetFloat("_Speed", 4);
    }

    // Update is called once per frame
    void Update()
    {
        playerMove = GetComponent<PlayerMove>().MoveVec;
        float 
            moveParameter,
            switchParameter;
        moveParameter = inObjectMaterial.GetFloat("_MoveParameter");
        switchParameter = inObjectMaterial.GetFloat("_SwitchParameter");
        if (inObjectNow)
        {
            if (moveParameter > -4 && switchParameter == 3)
            {
                moveParameter -= speed;
            }
            if (moveParameter < 4 && switchParameter == -5)
            {
                moveParameter += speed;
            }
            if (moveParameter <= -4 && switchParameter == 3)
            {
                inObjectMaterial.SetFloat("_SwitchParameter", -5);
                moveParameter = 4;
            }
        }
        else
        {
            if (moveParameter > -4 && switchParameter == -5)
            {
                moveParameter -= speed;
            }
            if (moveParameter < 4 && switchParameter == 3)
            {
                moveParameter += speed;
            }
            if (moveParameter <= -4 && switchParameter == -5)
            {
                inObjectMaterial.SetFloat("_SwitchParameter", 3);
                moveParameter = 4;
            }
        }
        inObjectMaterial.SetFloat("_MoveParameter", moveParameter);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Contains("Enemy"))
        {
            Finishd = Finish.gameOver;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Cube")
        {
            other.gameObject.tag = "HitCube";
            if (!inObjectNow)
            {
                inObject.transform.LookAt(new Vector3(transform.position.x + playerMove.x, transform.position.y,transform.position.z + playerMove.z));
            }
            inObjectNow = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "HitCube")
        {
            other.gameObject.tag = "Cube";
            inObjectNow = false;
            inObject.transform.LookAt(new Vector3(transform.position.x + playerMove.x, transform.position.y, transform.position.z + playerMove.z));
        }
    }
}