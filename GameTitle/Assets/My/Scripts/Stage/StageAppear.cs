﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StageAppear : MonoBehaviour
{
    public StageTable appearTable;
    private Controller player;
    private GameObject stage;
    private int stack;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerHand").GetComponent<Controller>();
        stack = player.ItemCount;
        for (int i = 0; i < appearTable.GetList().Count; i++)
        {
            for (int j = 0; j < appearTable.GetList()[i].stageObject.Length; j++)
            {
                stage = appearTable.GetList()[i].stageObject[j];
                stage.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //獲得アイテム数が変わったときにのみ使う
        //ステージをアクティブにする
        if (player.ItemCount != stack)
        {
            for (int i = 0; i < appearTable.GetList().Count; i++)
            {
                if (appearTable.GetList()[i].itemCount == player.ItemCount)
                {
                    for (int j = 0; j < appearTable.GetList()[i].stageObject.Length; j++)
                    {
                        stage = appearTable.GetList()[i].stageObject[j];
                        stage.SetActive(true);
                    }
                    stack = player.ItemCount;
                    break;
                }
            }
        }
    }
}
/// <summary>
/// Dictionaryの管理クラス
/// Listを受け取ってDictionaryにValueとして格納したりする所
/// </summary>
/// <typeparam name="Key"></typeparam>
/// <typeparam name="Value"></typeparam>
/// <typeparam name="Type"></typeparam>
[System.Serializable]
public class AppearTable<Key, Value, Type> where Type : GetKeyValue<Key, Value>
{
    [SerializeField]
    protected List<Type> stageList;
    protected Dictionary<Key, Value> stageTable;

    public AppearTable()
    {
        stageList = new List<Type>();
    }
    public Dictionary<Key, Value> GetTable()
    {
        if (stageTable == null)
            stageTable = Convert(stageList);
        return stageTable;
    }
    public List<Type> GetList()
    {
        return stageList;
    }
    static Dictionary<Key,Value> Convert(List<Type> list)
    {
        Dictionary<Key, Value> dic = new Dictionary<Key, Value>();
        foreach (GetKeyValue<Key,Value> pair in list)
        {
            dic.Add(pair.itemCount, pair.stageObject);
        }
        return dic;
    }
}
[System.Serializable]
public class GetKeyValue<Key, Value>
{
    public Key itemCount;
    public Value stageObject;
    public GetKeyValue(Key key, Value value)
    {
        itemCount = key;
        stageObject = value;
    }
    public GetKeyValue(KeyValuePair<Key, Value> pair)
    {
        itemCount = pair.Key;
        stageObject = pair.Value;
    }
}

/// <summary>
/// 
/// </summary>
[System.Serializable]
public class StageTable : AppearTable<int, GameObject[], StagePair>
{

}
[System.Serializable]
public class StagePair : GetKeyValue<int, GameObject[]>
{
    public StagePair(int key, GameObject[] value) : base(key, value)
    {
    }
}