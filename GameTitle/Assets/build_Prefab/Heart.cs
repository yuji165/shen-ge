﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : CreateParent
{
    private const float goldenRatioY = 1f;
    private const float goldenRatioX = 1.6180339887499f;
    private const float percentage = 1 / (goldenRatioY + goldenRatioX);
    public float distance;
    private int oldCreateCount;
    private float oldDistance;
    protected override bool IfUpdata()
    {
        return oldCreateCount != createCount || oldDistance != distance;
    }
    protected override void Create()
    {
        ListReset();
        for (float lr = 1; lr > -2; lr -= 2)
        {
            for (float ud = 1; ud > -2; ud -= 2)
            {
                float createCountUD = ud == 1 ? createCount * 0.4f : createCount * 0.6f;
                for (float i = 0.5f; i < createCountUD + 1; i++)
                {
                    Vector3 pos = SetPos(lr * Mathf.Sin(Mathf.PI / 2 / createCountUD * i), ud, lr) * distance;
                    Vector3 a = transform.TransformDirection(Vector3.left) * pos.x;
                    Vector3 b = transform.TransformDirection(Vector3.up) * pos.y;
                    Vector3 c = transform.TransformDirection(Vector3.forward) * pos.z;
                    Vector3 abc = a + b + c;
                    objList.Add(Instantiate(obj, transform.position + abc, Quaternion.identity));
                }
            }
        }
        oldCreateCount = this.createCount;
        oldDistance = distance;
    }
    private Vector3 SetPos(float x, float ud, float lr)
    {
        Vector3 pos = Vector3.zero;

        pos.x = x;
        float a = ud * Mathf.Sqrt(1f - x * x);
        float b = Mathf.Sqrt(Mathf.Abs(x));

        pos.z = a + b;
        return pos;
    }
}
