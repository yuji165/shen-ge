﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : CreateParent
{
    private const float goldenRatioY = 1f;
    private const float goldenRatioX = 1.6180339887499f;
    private const float percentage = 1 / (goldenRatioY + goldenRatioX);
    public Vector3 scale;
    public float scaleYMin;
    public float distance;
    public float defAngle;
    private int oldCreateCount;
    private float oldDistance;
    private Vector3 oldScale;
    protected override bool IfUpdata()
    {
        return oldCreateCount != createCount || oldDistance != distance || oldScale != scale;
    }
    protected override void Create()
    {
        ListReset();
        Vector2[] vertex = new Vector2[10];
        int oneang = (360 / 5);
        for (int i = 0; i < 5; i++)
        {
            vertex[i * 2] = new Vector2(
                Mathf.Floor(Mathf.Cos((90 + defAngle + oneang * i) * Mathf.Deg2Rad) * 100) / 100 * distance,
                Mathf.Floor(Mathf.Sin((90 + defAngle + oneang * i) * Mathf.Deg2Rad) * 100) / 100 * distance);
        }
        for (int i = 0; i < 5; i++)
        {
            Vector2 dist = (vertex[((i + 2) % 5) * 2] - vertex[i * 2]) * percentage;
            vertex[i * 2 + 1] = vertex[i * 2] + dist;
        }
        int createCount = this.createCount / 10;
        float maxDist = Vector3.Distance(vertex[0],vertex[1]);
        for (int i = 0; i < 10; i++)
        {
            Vector2 dist = vertex[(i + 1) % 10] - vertex[i];
            for (int k = 0; k < createCount; k++)
            {
                Vector3 pos = new Vector3(vertex[i].x + dist.x / createCount * k, 0, vertex[i].y + dist.y / createCount * k);
                objList.Add(Instantiate(obj, transform.position + pos, Quaternion.identity));

                float d = Vector3.Distance(objList[objList.Count - 1].transform.position,
                    transform.position + new Vector3(vertex[i + 1 - i % 2].x, 0, vertex[i + 1 - i % 2].y));
                float scaleY = scale.y - scaleYMin;
                objList[objList.Count - 1].transform.localScale = new Vector3(scale.x, scaleYMin + scaleY - scaleY * (d / maxDist), scale.z);
            }
        }
        oldCreateCount = this.createCount;
        oldDistance = distance;
        oldScale = scale;
    }
}
